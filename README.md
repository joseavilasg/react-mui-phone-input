# React MUI Phone Input

React component to format phone number, based on MUI..

[![npm version](https://img.shields.io/npm/v/react-mui-phone-input.svg?style=flat)](https://www.npmjs.com/package/react-mui-phone-input)
[![npm downloads](https://img.shields.io/npm/dm/react-mui-phone-input.svg?style=flat)](https://www.npmjs.com/package/react-mui-phone-input)
[![MRs Welcome](https://img.shields.io/badge/MRs-welcome-brightgreen.svg)](https://gitlab.com/joseavilasg/react-mui-phone-input/-/merge_requests)

This is a personal proyect. If you want to contribute, feel free to do it. I'll be improving this project and will generate the documentation soon.

## Installation

```shell-script
npm i react-mui-phone-input
```

#### [Demo](https://joseavilasg.gitlab.io/react-mui-phone-input/)

## License

[![GitLab license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/joseavilasg/react-mui-phone-input/-/blob/main/LICENSE)
