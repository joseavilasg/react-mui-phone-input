import { isPossiblePhoneNumber, isValidPhoneNumber, type CountryCode } from "libphonenumber-js";
import PhoneInputMUI from "@/PhoneInputMUI";
import type { CountryName } from "@/types";
import "./src/styles/index.sass";

export { PhoneInputMUI, isPossiblePhoneNumber, isValidPhoneNumber };
export type { CountryName, CountryCode };
