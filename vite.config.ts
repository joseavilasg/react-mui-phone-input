import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";

import { resolve } from "path";
import svgr from "vite-plugin-svgr";
import eslintPlugin from "vite-plugin-eslint";

import dts from "vite-plugin-dts";
import tsConfigPaths from "vite-tsconfig-paths";
import * as packageJson from "./package.json";

const packageName = "react-mui-phone-input";

const projectRootDir = resolve(__dirname);

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [
		dts({
			include: ["."],
			exclude: ["./src/App.tsx", "./src/main.tsx", "./node_modules"],
		}),
		react(),
		tsConfigPaths(),
		svgr(),
		eslintPlugin(),
	],
	resolve: {
		alias: {
			"@": resolve(projectRootDir, "src"),
		},
	},
	build: {
		outDir: "dist",
		sourcemap: false,
		target: "esnext",
		minify: "esbuild",
		cssMinify: "esbuild",
		lib: {
			entry: resolve(".", "index.ts"),
			name: packageName,
			formats: ["es", "umd"],
			fileName: (format) => `${packageName}.${format}.js`,
		},
		copyPublicDir: false,
		rollupOptions: {
			external: [...Object.keys(packageJson.peerDependencies)],
		},
	},
});
