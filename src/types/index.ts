import { SxProps, TextFieldVariants, Theme } from "@mui/material";

export type CountryName = { iso2: string; name: string };
type FormChangeAutocompleteEvent = {
	(event: React.SyntheticEvent<Element, Event>, newValue: CountryName | null): void;
};
type FormChangeInputAutocompleteEvent = {
	(event: React.SyntheticEvent<Element, Event>, newValue: string | null): void;
};

export type CountryAutoCompleteProps = {
	variant: TextFieldVariants;
	placeholder: string;
	options: CountryName[];
	getOptionLabel: (option: CountryName) => string;
	isOptionEqualToValue: (option: CountryName, value: CountryName) => boolean;
	value: CountryName;
	onChange: FormChangeAutocompleteEvent;
	onInputChange?: FormChangeInputAutocompleteEvent;
	onBlur?: React.FocusEventHandler<HTMLDivElement>;
	onOptionClick?: React.MouseEventHandler<HTMLLIElement>;
	sx?: SxProps<Theme>;
};
