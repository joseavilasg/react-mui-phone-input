import { isPossiblePhoneNumber } from "libphonenumber-js";
import PhoneInputMUI from "./PhoneInputMUI";
import "./styles/index.sass";
import { useEffect, useRef, useState } from "react";

const App = () => {
	const phoneInputRef = useRef<HTMLInputElement>(null);
	const [value, setValue] = useState("");
	const [error, setError] = useState(false);
	const [errorText, setErrorText] = useState("");

	useEffect(() => {
		console.log({ value });
	}, [value]);

	return (
		<>
			<PhoneInputMUI
				name="Phone input"
				value="+51 999 999 999"
				ref={phoneInputRef}
				variant="outlined"
				lang="en"
				label="Phone number"
				required
				helperText={errorText}
				forceInternationalFormat
				defaultCountryIso2="PE"
				error={error}
				onChange={({ value, formattedValue }) => {
					setValue(value);
					setError(!isPossiblePhoneNumber(formattedValue));
					setErrorText(
						!isPossiblePhoneNumber(formattedValue) ? "This phone number is incorrect." : "",
					);
				}}
				onBlur={() => console.log("Blur")}
			/>
		</>
	);
};

export default App;
