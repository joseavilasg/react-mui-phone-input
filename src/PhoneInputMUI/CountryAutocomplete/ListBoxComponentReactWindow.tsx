import * as React from "react";
import useMediaQuery from "@mui/material/useMediaQuery";
import { SxProps, Theme, useTheme } from "@mui/material/styles";
import { VariableSizeList } from "react-window";
import { Box, MenuItem } from "@mui/material";
import { CountryName } from "@/types";
import { CountryCode, getCountryCallingCode } from "libphonenumber-js";

const LISTBOX_PADDING = 0; // px
const ROWS_NUMBER = 4; // px

interface IRenderRow {
	data: DataSet[];
	index: number;
	style: React.CSSProperties;
}

export type OptionProps = React.HTMLAttributes<HTMLLIElement> & { sx?: SxProps<Theme> };

type DataSet = [OptionProps, CountryName];

const renderRow: React.FC<IRenderRow> = (props) => {
	const { data, index, style } = props;
	const dataSet = data[index];
	const inlineStyle = {
		...style,
		top: (style?.top as number) + LISTBOX_PADDING,
	};

	const label = `${dataSet[1]?.name} (${dataSet[1]?.iso2}) +${getCountryCallingCode(
		dataSet[1].iso2 as CountryCode,
	)}`;

	return (
		<MenuItem
			component="li"
			style={inlineStyle}
			{...dataSet[0]}
			sx={{
				"& > img": { mr: 2, flexShrink: 0 },
				...dataSet[0].sx,
				maxWidth: 1,
				overflow: "hidden",
			}}
			title={label}
		>
			{dataSet[1]?.iso2 && (
				// <img
				// 	loading="lazy"
				// 	width="20"
				// 	src={`https://flagcdn.com/${dataSet[1]?.iso2.toLowerCase()}.svg`}
				// 	srcSet={`https://flagcdn.com/${dataSet[1]?.iso2.toLowerCase()}.svg 2x`}
				// 	alt=""
				// />
				<Box
					className={`flag ${dataSet[1]?.iso2.toLowerCase()}`}
					sx={{ width: 20, height: 20, mr: 2, flexShrink: 0 }}
				/>
			)}
			{label}
		</MenuItem>
	);
};

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef<HTMLDivElement>(function OuterElementType(props, ref) {
	const outerProps = React.useContext(OuterElementContext);
	return <div ref={ref} {...props} {...outerProps} />;
});

function useResetCache(data: number) {
	const ref = React.useRef<VariableSizeList>(null);
	React.useEffect(() => {
		if (ref.current != null) {
			ref.current.resetAfterIndex(0, true);
		}
	}, [data]);
	return ref;
}

type ListboxComponentProps = {
	data: DataSet[];
	options: CountryName[];
	selectedCountry: CountryName;
	listBoxProps: React.HTMLAttributes<HTMLUListElement>;
};

export type ListboxComponentRef = React.MutableRefObject<HTMLDivElement>;

// Adapter for react-window
const ListboxComponent = React.forwardRef<ListboxComponentRef, ListboxComponentProps>(
	function ListboxComponent(props, ref) {
		const { data, selectedCountry, listBoxProps, options } = props;

		const theme = useTheme();
		const smUp = useMediaQuery(theme.breakpoints.up("xs"), {
			noSsr: true,
		});

		const itemCount = data.length;
		const itemSize = smUp ? 36 : 36;

		const getChildSize = (child: DataSet) => {
			if (Object.prototype.hasOwnProperty.call(child, "group")) {
				// Never executes, because group is not a property of IDataSet. Anyway, I let this for future use.
				// if (child.hasOwnProperty("group")) {
				return 48;
			}

			return itemSize;
		};

		const getHeight = () => {
			if (itemCount > ROWS_NUMBER) {
				return ROWS_NUMBER * itemSize;
			}
			return data.map(getChildSize).reduce((a, b) => a + b, 0);
		};

		const gridRef = useResetCache(itemCount);

		const selectedCountryIndex = options.findIndex(
			(option) => option.iso2 === selectedCountry.iso2,
		);

		return (
			<Box ref={ref as React.ForwardedRef<HTMLDivElement>}>
				<OuterElementContext.Provider value={listBoxProps}>
					<VariableSizeList
						itemData={data}
						height={getHeight() + 2 * LISTBOX_PADDING}
						width="100%"
						ref={gridRef}
						outerElementType={OuterElementType}
						innerElementType="ul"
						itemSize={(index) => getChildSize(data[index])}
						overscanCount={5}
						itemCount={itemCount}
						initialScrollOffset={(selectedCountryIndex - 1) * itemSize}
					>
						{renderRow}
					</VariableSizeList>
				</OuterElementContext.Provider>
			</Box>
		);
	},
);

export default ListboxComponent;
