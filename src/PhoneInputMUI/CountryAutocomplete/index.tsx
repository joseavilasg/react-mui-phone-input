import React, { useEffect, useRef } from "react";
import ListboxComponent, { OptionProps } from "./ListBoxComponentReactWindow";
import { TextField, useAutocomplete, Box } from "@mui/material";
import { useTheme } from "@mui/material/styles";

import { CountryAutoCompleteProps, CountryName } from "@/types";

const grey = {
	50: "#f6f8fa",
	100: "#eaeef2",
	200: "#d0d7de",
	300: "#afb8c1",
	400: "#8c959f",
	500: "#6e7781",
	600: "#57606a",
	700: "#424a53",
	800: "#32383f",
	900: "#24292f",
};

const CountryAutocomplete: React.FC<CountryAutoCompleteProps> = (props) => {
	const {
		variant,
		placeholder,
		sx,
		options,
		getOptionLabel,
		isOptionEqualToValue,
		onChange,
		onInputChange,
		onOptionClick,
		value,
	} = props;

	const textFieldRef = useRef<HTMLInputElement>(null);

	const theme = useTheme();

	useEffect(() => {
		textFieldRef.current?.focus();
		setTimeout(() => {
			textFieldRef.current?.select();
		}, 0.0001);
	}, []);

	const { getRootProps, getListboxProps, getInputProps, getOptionProps, groupedOptions } =
		useAutocomplete({
			options,
			getOptionLabel,
			isOptionEqualToValue,
			onChange,
			onInputChange,
			open: true,
			autoHighlight: true,
			value,
		});

	return (
		<>
			<Box
				{...getRootProps()}
				sx={{
					"& ul": {
						padding: 0,
						overflow: "hidden",
					},
					[`& .MuiMenuItem-root`]: {
						minHeight: "auto",
					},
					"& .MuiTextField-root ": {
						mb: 0.5,
					},
					width: 1,
					...sx,
				}}
			>
				<TextField
					fullWidth
					inputRef={textFieldRef}
					inputProps={getInputProps()}
					variant={variant}
					placeholder={placeholder}
					sx={{
						pl: 2,
						pr: 4,
						py: 1,
						boxSizing: "border-box",
						...(() => {
							if (variant !== "outlined") {
								return {
									"& input": {
										pt: "12px",
									},
								};
							}
							return {};
						})(),
					}}
					InputProps={{ sx: { height: 36 } }}
				/>
				<ListboxComponent
					selectedCountry={value}
					listBoxProps={getListboxProps()}
					options={groupedOptions as CountryName[]}
					data={groupedOptions.map((option, index) => {
						const optionProps = getOptionProps({ option: option as CountryName, index });
						const customOnClick = (e: React.MouseEvent<HTMLLIElement>) => {
							onOptionClick?.(e);
							optionProps.onClick?.(e);
						};
						const newOptionProps: OptionProps = {
							...optionProps,
							onClick: customOnClick,
							sx: {
								listStyle: "none",
								cursor: "default",
								"&:hover": {
									cursor: "pointer",
								},
								"&[aria-selected=true]": {
									backgroundColor: `${
										theme.palette.mode === "dark" ? "#90caf93d" : "#1976d21f"
									}`,
								},
								"&.Mui-focused": {
									backgroundColor: `${
										theme.palette.mode === "dark" ? grey[800] : grey[100]
									}`,
								},
								"&.Mui-focusVisible": {
									backgroundColor: `${
										theme.palette.mode === "dark" ? grey[800] : grey[100]
									}`,
								},
								"&[aria-selected=true].Mui-focused": {
									backgroundColor: `${
										theme.palette.mode === "dark" ? "#90caf93d" : "#1976d21f"
									}`,
								},
								"&[aria-selected=true].Mui-focusVisible": {
									backgroundColor: `${
										theme.palette.mode === "dark" ? "#90caf93d" : "#1976d21f"
									}`,
								},
							},
						};
						return [newOptionProps, option as CountryName];
					})}
				/>
			</Box>
		</>
	);
};

export default CountryAutocomplete;
