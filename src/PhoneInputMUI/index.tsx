import React, { useState, useEffect, useRef } from "react";
import {
	InputAdornment,
	TextField,
	InputProps,
	OutlinedInputProps,
	FilledInputProps,
	IconButton,
	Box,
} from "@mui/material";
import { useForkRef } from "@mui/material/utils";
import {
	CountryCode,
	formatIncompletePhoneNumber,
	getCountryCallingCode,
	getExampleNumber,
	validatePhoneNumberLength,
} from "libphonenumber-js";
import dialingCode from "@/const/dialingCode";
import countriesNameEn from "../const/countriesName/en";
import countriesNameEs from "../const/countriesName/es";
import CountryAutocomplete from "./CountryAutocomplete";
import { CountryName } from "@/types";

import examples from "libphonenumber-js/mobile/examples";

import Menu from "@mui/material/Menu";

type Variant = "standard" | "outlined" | "filled";

type PhoneInputMUIProps = {
	variant: Variant;
	value?: string;
	lang: "en" | "es";
	name?: string;
	label?: string;
	helperText?: string;
	error?: boolean;
	forceInternationalFormat?: boolean;
	defaultCountryIso2?: CountryCode;
	customPlaceholder?: string;
	onChange?: ({
		e,
		value,
		formattedValue,
		selectedCountry,
	}: {
		e: React.ChangeEvent<HTMLInputElement>;
		value: string;
		formattedValue: string;
		selectedCountry: CountryName;
	}) => void;
	onBlur?: React.FocusEventHandler<HTMLInputElement>;
	required?: boolean;
};

const PhoneInputMUI = React.forwardRef<HTMLInputElement, PhoneInputMUIProps>(function (props, ref) {
	const {
		variant,
		value,
		lang,
		error,
		helperText,
		name,
		label,
		forceInternationalFormat = true,
		defaultCountryIso2 = "US",
		customPlaceholder,
		onChange,
		onBlur,
		required,
	} = props;
	const countriesName = (() => {
		if (lang === "en") {
			return countriesNameEn;
		}
		if (lang === "es") {
			return countriesNameEs;
		}
		return countriesNameEn;
	})();

	function getCountryName(string: CountryCode) {
		return countriesName.find((country) => country.iso2 === string)?.name;
	}

	const defaultSelectedCountry = (() => {
		const countryName = getCountryName(defaultCountryIso2);
		if (countryName) {
			return {
				iso2: defaultCountryIso2,
				name: countryName,
			};
		}

		return {
			iso2: "US",
			name: "United States",
		};
	})();

	const [selectedCountry, setSelectedCountry] = useState<CountryName>(defaultSelectedCountry);
	const [phoneValue, _setPhoneValue] = useState(value || "");
	const phoneValueRef = useRef(phoneValue);
	const setPhoneValue = (state: string) => {
		phoneValueRef.current = state;
		_setPhoneValue(state);
	};

	useEffect(() => {
		if (value) {
			setPhoneValue(value);
		}
	}, [value]);

	const inputRef = useRef<HTMLInputElement>(null);
	const handleRef = useForkRef(inputRef, ref);

	const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
	const open = Boolean(anchorEl);

	const handleOpen = (event: React.MouseEvent<HTMLButtonElement>) => {
		setAnchorEl(event.currentTarget);
	};
	const handleClose = () => {
		setAnchorEl(null);
		setTimeout(() => {
			if (anchorEl) {
				inputRef.current?.focus();
			}
		}, 0.0001);
	};

	useEffect(() => {
		if (forceInternationalFormat) {
			if (
				getPhoneCodeFromInput(phoneValueRef.current) !==
				getCountryCallingCode(selectedCountry.iso2 as CountryCode)
			) {
				setPhoneValue(`+${getCountryCallingCode(selectedCountry.iso2 as CountryCode)}`);
			}
		}
		handleClose();
	}, [selectedCountry]);

	function getPhoneCodeFromInput(text: string) {
		const indexSpace = text.indexOf(" ");
		const phoneCode = indexSpace === -1 ? text : text.slice(0, indexSpace);
		const indexPlus = phoneCode.indexOf("+");
		const phoneCodeWithoutPlus = indexPlus === -1 ? phoneCode : phoneCode.slice(1);
		return phoneCodeWithoutPlus;
	}

	function handlePlusSign(text: string) {
		if (text.length === 0) {
			return "";
		}
		if (text.length === 1) {
			if (text === "+") {
				return text;
			}
			return `+${text}`;
		}
		const indexPlus = text.indexOf("+");
		const phoneCodeWithPlus = (() => {
			if (indexPlus === -1) {
				return `+${text}`;
			}
			return text;
		})();
		return phoneCodeWithPlus;
	}

	const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const inputValue = e.target.value;
		// if (
		// 	phoneValueRef.current === "" &&
		// 	inputValue.length >= 2 &&
		// 	inputValue.indexOf("+") === -1
		// ) {
		// 	// TODO: Build a function, if possible, to check a national number, and try to find the country which it belongs to
		// 	console.log({
		// 		inputValue,
		// 		countries: parsePhoneNumber(
		// 			inputValue,
		// 			selectedCountry.iso2 as CountryCode,
		// 		).getPossibleCountries(),
		// 	});
		// }
		// Let no write more than max length
		const validation = validatePhoneNumberLength(
			e.target.value,
			selectedCountry.iso2 as CountryCode,
		);
		if (!(validation === "TOO_LONG")) {
			if (forceInternationalFormat) {
				setPhoneValue(
					formatIncompletePhoneNumber(
						handlePlusSign(inputValue),
						selectedCountry.iso2 as CountryCode,
					),
				);
			} else {
				setPhoneValue(
					formatIncompletePhoneNumber(inputValue, selectedCountry.iso2 as CountryCode),
				);
			}
		}

		if (forceInternationalFormat) {
			// Select country when a code match
			const phoneCode = getPhoneCodeFromInput(phoneValueRef.current);
			const countryCode = dialingCode.find((code) => code.phoneCode === phoneCode)
				?.iso2 as CountryCode;
			const countryName = getCountryName(countryCode);
			if (countryName) {
				setSelectedCountry({ iso2: countryCode, name: countryName });
			}
		}

		onChange?.({
			e,
			value: phoneValueRef.current.replaceAll(" ", ""),
			formattedValue: phoneValueRef.current,
			selectedCountry,
		});
	};

	const FlagAdorment = (props: Omit<React.HTMLAttributes<HTMLDivElement>, "className">) => {
		return (
			<div
				{...props}
				className={`flag flag-arrow ${selectedCountry.iso2.toLowerCase()}`}
				title={`${selectedCountry.name}: +${getCountryCallingCode(
					selectedCountry.iso2 as CountryCode,
				)}`}
			/>
		);
	};

	const muiInputProps: OutlinedInputProps | InputProps | FilledInputProps = {
		startAdornment: (
			<InputAdornment position="start">
				<IconButton onClick={handleOpen} sx={{ ml: -1 }}>
					<FlagAdorment />
				</IconButton>
			</InputAdornment>
		),
	};

	return (
		<Box sx={{ width: 1 }}>
			<TextField
				type="tel"
				autoComplete="mobile tel"
				fullWidth
				name={name}
				required={required}
				variant={variant}
				label={label}
				error={error}
				helperText={helperText}
				placeholder={(() => {
					if (customPlaceholder !== undefined) {
						return customPlaceholder;
					}

					const exampleNumber = getExampleNumber(
						selectedCountry.iso2 as CountryCode,
						examples,
					)?.format(forceInternationalFormat ? "INTERNATIONAL" : "NATIONAL");

					if (exampleNumber) return exampleNumber;

					return "";
				})()}
				InputProps={muiInputProps}
				inputProps={{ sx: { pl: "14px", ml: "-10px" } }}
				inputRef={handleRef}
				value={phoneValueRef.current}
				onChange={handleInputChange}
				onBlur={onBlur}
			/>
			<Menu anchorEl={anchorEl} open={open} onClose={handleClose} elevation={1}>
				<CountryAutocomplete
					sx={{ p: 0, width: 350 }}
					variant={variant}
					placeholder="Search country"
					options={countriesName}
					getOptionLabel={(option) => {
						return option.name;
					}}
					isOptionEqualToValue={(option, value) => {
						return option.name === value.name;
					}}
					value={selectedCountry}
					onChange={(_, newValue) => {
						newValue ? setSelectedCountry(newValue) : undefined;
					}}
				/>
			</Menu>
		</Box>
	);
});

export default PhoneInputMUI;
